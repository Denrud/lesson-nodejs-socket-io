console.log('test')

const socket = new WebSocket('ws://localhost:8080');

const formData = document.forms.testform

formData.addEventListener('submit', (ev) => {
    ev.preventDefault();
    const data = new FormData(ev.target);
    const getData = data.get('number');
    socket.send(getData);
});

socket.addEventListener('message', (ev) => {
    console.log('Message from server', ev.data );
});
